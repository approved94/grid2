package com.approved.StartMenu;

import com.approved.Helper;
import com.approved.SelectWorld.SelectWorldScene;
import com.approved.input.InputElement;
import com.approved.input.InputManager;
import com.approved.options.OptionsScene;
import com.approved.player.Player;
import com.approved.scenes.Scene;
import com.approved.scenes.SceneManager;
import com.approved.world.Map;
import com.approved.world.World;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

public class StartMenu extends Scene {
	
	public StartMenu() {
		super();
		World.setScale(1f);
		World.OFFSET_X = 0;
		World.OFFSET_Y = 0;
		World.MAP = new Map();
		World.Init();
		BuildMenu();
		
		System.out.println(Player.GetInstance().ToJson());
	}
	
	private void BuildMenu() {
		// Build the start menu below
		InputManager.RemoveListener(World.getInstance()); // Make sure the world in the background is just that, a background
		BuildElements();
	}



	private void BuildElements() {
		// LOGO:
		InputElement logo = new InputElement();
		logo.setTexture(Helper.GetTexture("gridtowerdefence"));
		logo.SetWidthScreen(60);
		logo.MarginTop(20);
		logo.CenterX();
		AddElement(logo);

		// Start Game
		InputElement SelectWorld = new InputElement();
		SelectWorld.setTexture(Helper.GetTexture("startgame"));
		SelectWorld.SetWidthScreen(40);
		SelectWorld.CenterX();
		SelectWorld.MarginTopPercentage(50);
		SelectWorld.processor = new InputProcessor() {

			@Override
			public boolean touchUp(int screenX, int screenY, int pointer, int button) {
				SceneManager.setScene(new SelectWorldScene());
				return false;
			}

			@Override
			public boolean touchDragged(int screenX, int screenY, int pointer) {
				
				return false;
			}

			@Override
			public boolean touchDown(int screenX, int screenY, int pointer,
					int button) {
				
				return false;
			}

			@Override
			public boolean scrolled(int amount) {
				
				return false;
			}

			@Override
			public boolean mouseMoved(int screenX, int screenY) {
				
				return false;
			}

			@Override
			public boolean keyUp(int keycode) {
				
				return false;
			}

			@Override
			public boolean keyTyped(char character) {
				
				return false;
			}

			@Override
			public boolean keyDown(int keycode) {
				
				return false;
			}
		};
		AddElement(SelectWorld);

		// Options
		InputElement Options = new InputElement();
		Options.setTexture(Helper.GetTexture("options"));
		Options.SetWidthScreen(40);
		Options.CenterX();
		Options.MarginTopPercentage(62);
		Options.processor = new InputProcessor() {

			@Override
			public boolean touchUp(int screenX, int screenY, int pointer,
					int button) {
				SceneManager.setScene(new OptionsScene());
				return false;
			}

			@Override
			public boolean touchDragged(int screenX, int screenY, int pointer) {
				
				return false;
			}

			@Override
			public boolean touchDown(int screenX, int screenY, int pointer,
					int button) {
				
				return false;
			}

			@Override
			public boolean scrolled(int amount) {
				
				return false;
			}

			@Override
			public boolean mouseMoved(int screenX, int screenY) {
				
				return false;
			}

			@Override
			public boolean keyUp(int keycode) {
				
				return false;
			}

			@Override
			public boolean keyTyped(char character) {
				
				return false;
			}

			@Override
			public boolean keyDown(int keycode) {
				
				return false;
			}
		};
		AddElement(Options);

		// Exit
		InputElement Exit = new InputElement();
		Exit.setTexture(Helper.GetTexture("exit"));
		Exit.SetWidthScreen(40);
		Exit.CenterX();
		Exit.MarginTopPercentage(74);
		Exit.processor = new InputProcessor() {

			@Override
			public boolean touchUp(int screenX, int screenY, int pointer,
					int button) {
				Gdx.app.exit();
				return false;
			}

			@Override
			public boolean touchDragged(int screenX, int screenY, int pointer) {
				
				return false;
			}

			@Override
			public boolean touchDown(int screenX, int screenY, int pointer,
					int button) {
				
				return false;
			}

			@Override
			public boolean scrolled(int amount) {
				
				return false;
			}

			@Override
			public boolean mouseMoved(int screenX, int screenY) {
				
				return false;
			}

			@Override
			public boolean keyUp(int keycode) {
				
				return false;
			}

			@Override
			public boolean keyTyped(char character) {
				
				return false;
			}

			@Override
			public boolean keyDown(int keycode) {
				
				return false;
			}
		};
		AddElement(Exit);
	}



	@Override
	public void Render(ShapeRenderer shape, SpriteBatch sprite) {
		World.RenderBackground(shape, sprite);
		sprite.end(); // End the sprite batch before we render with the shape so it renders the right way
		
		Helper.EnableBlend(); // Enable blending as we are going to render some transparency here
		shape.begin(ShapeType.Filled);
		shape.setColor(0, 0, 0, 0.4f);
		shape.rect(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		shape.end();
		Helper.DisableBlend(); // Disable blending, no longer needed
		sprite.begin();
		
		super.Render(shape, sprite);
	}
	
}
