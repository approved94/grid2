package com.approved.options;

import com.approved.Helper;
import com.approved.StartMenu.StartMenu;
import com.approved.input.InputButton;
import com.approved.scenes.Scene;
import com.approved.scenes.SceneManager;
import com.approved.world.World;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

public class OptionsScene extends Scene {

	public OptionsScene() {
		super();
		BuildMenu();
	}
	
	private void BuildMenu() {
		InputButton button = new InputButton();
		button.Text = "Back";
		button.SizeFromText();
		button.Center();
		button.processor = new InputProcessor() {
			
			@Override
			public boolean touchUp(int screenX, int screenY, int pointer, int button) {
				SceneManager.setScene(new StartMenu());
				return false;
			}
			
			@Override
			public boolean touchDragged(int screenX, int screenY, int pointer) {
				return false;
			}
			
			@Override
			public boolean touchDown(int screenX, int screenY, int pointer, int button) {
				return false;
			}
			
			@Override
			public boolean scrolled(int amount) {
				return false;
			}
			
			@Override
			public boolean mouseMoved(int screenX, int screenY) {
				return false;
			}
			
			@Override
			public boolean keyUp(int keycode) {
				return false;
			}
			
			@Override
			public boolean keyTyped(char character) {
				return false;
			}
			
			@Override
			public boolean keyDown(int keycode) {
				return false;
			}
		};
		AddElement(button);
	}

	@Override
	public void Render(ShapeRenderer shape, SpriteBatch sprite) {
		World.RenderBackground(shape, sprite);
		sprite.end(); // End the sprite batch before we render with the shape so it renders the right way
		
		Helper.EnableBlend(); // Enable blending as we are going to render some transparency here
		shape.begin(ShapeType.Filled);
		shape.setColor(0, 0, 0, 0.4f);
		shape.rect(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		shape.end();
		Helper.DisableBlend(); // Disable blending, no longer needed
		sprite.begin();
		
		super.Render(shape, sprite); // This will render all buttons and elements
	}
	
}
