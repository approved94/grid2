package com.approved.scenes;

import java.util.ArrayList;

import com.approved.input.InputElement;
import com.approved.input.InputManager;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public abstract class Scene implements InputProcessor {
	private ArrayList<InputElement> elements = new ArrayList<InputElement>();
	
	public Scene() {
		InputManager.AddListener(this);
	}
	
	public int getMouseY() {
		return Gdx.graphics.getHeight() - Gdx.input.getY();
	}
	
	public int getMouseX() {
		return Gdx.input.getX();
	}
	
	public void Render(ShapeRenderer shape, SpriteBatch sprite) {
		for(int i = 0; i < elements.size(); i++) {
			elements.get(i).render(shape, sprite);
		}
	}
	
	public void AddElement(InputElement element) {
		while(!elements.contains(element)) {
			elements.add(element);
		}
	}
	
	public void RemoveElement(InputElement element) {
		while(elements.contains(element)) {
			elements.remove(element);
			InputManager.RemoveListener(element);
		}
	}
	
	public void Shutdown() {
		InputManager.RemoveListener(this);
		for(int i = 0; i < elements.size(); i++) {
			RemoveElement(elements.get(i));
		}
	}
	
	@Override
	public boolean keyDown(int keycode) {
		for(int i = 0; i < elements.size(); i++) {
			boolean res = elements.get(i).keyDown(keycode);
			if(res) return true;
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		for(int i = 0; i < elements.size(); i++) {
			boolean res = elements.get(i).keyUp(keycode);
			if(res) return true;
		}
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		for(int i = 0; i < elements.size(); i++) {
			boolean res = elements.get(i).keyTyped(character);
			if(res) return true;
		}
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		for(int i = 0; i < elements.size(); i++) {
			if(!elements.get(i).Within(getMouseX(), getMouseY())) continue;
			boolean res = elements.get(i).touchDown(screenX, screenY, pointer, button);
			if(res) return true;
		}
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		for(int i = 0; i < elements.size(); i++) {
			if(!elements.get(i).Within(getMouseX(), getMouseY())) continue;
			boolean res = elements.get(i).touchUp(screenX, screenY, pointer, button);
			if(res) return true;
		}
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		for(int i = 0; i < elements.size(); i++) {
			boolean res = elements.get(i).touchDragged(screenX, screenY, pointer);
			if(res) return true;
		}
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		for(int i = 0; i < elements.size(); i++) {
			boolean res = elements.get(i).mouseMoved(screenX, screenY);
			if(res) return true;
		}
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		for(int i = 0; i < elements.size(); i++) {
			boolean res = elements.get(i).scrolled(amount);
			if(res) return true;
		}
		return false;
	}
	
	public ArrayList<InputElement> GetElements() {
		return elements;
	}
	
}
