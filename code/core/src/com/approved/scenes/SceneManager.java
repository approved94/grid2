package com.approved.scenes;

import com.approved.input.InputManager;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class SceneManager {
	private static Scene ActiveScene = null;
	
	public static void setScene(Scene scene) {
		if(ActiveScene != null) ActiveScene.Shutdown();
		ActiveScene = scene;
		
		if(ActiveScene instanceof InputProcessor) InputManager.AddListener(ActiveScene);
	}
	
	public static void RemoveScene() {
		if(ActiveScene != null) ActiveScene.Shutdown();
		ActiveScene = null;
	}
	
	public static void Render(ShapeRenderer shape, SpriteBatch sprite) {
		if(ActiveScene != null) ActiveScene.Render(shape, sprite);
	}
	
}
