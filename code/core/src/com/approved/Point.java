package com.approved;

public class Point {

	public int x, y;

	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public Point(float x, float y) {
		this.x = (int)x;
		this.y = (int)y;
	}
	
	@Override
	public String toString() {
		return "Point: (" + x + ", " + y + ")";
	}
	
	public Point() {
		// This is used to allow for json-loading of points
	}

}
