package com.approved.SelectWorld;

import java.util.ArrayList;

import com.approved.GridTD2;
import com.approved.Helper;
import com.approved.Server;
import com.approved.StartMenu.StartMenu;
import com.approved.input.InputElement;
import com.approved.scenes.Scene;
import com.approved.scenes.SceneManager;
import com.approved.world.Map;
import com.approved.world.World;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

public class SelectWorldScene extends Scene {
	public static int WorldIndex = 0;
	public static ArrayList<Map> maps;
	private InputElement Next, Prev, StartGame;
	
	public SelectWorldScene() {
		maps = Server.GetMaps();
		InitWorlds();
		
		Next = new InputElement();
		Next.setTexture(Helper.GetTexture("next"));
		Next.SizeFromImage();
		Next.MarginRight(20);
		Next.CenterY();
		Next.processor = new InputProcessor() {
			
			@Override
			public boolean touchUp(int screenX, int screenY, int pointer, int button) {
				
				return false;
			}
			
			@Override
			public boolean touchDragged(int screenX, int screenY, int pointer) {
				
				return false;
			}
			
			@Override
			public boolean touchDown(int screenX, int screenY, int pointer, int button) {
				if(WorldIndex + 1 < maps.size()) {
					WorldIndex++;
				}
				return false;
			}
			
			@Override
			public boolean scrolled(int amount) {
				
				return false;
			}
			
			@Override
			public boolean mouseMoved(int screenX, int screenY) {
				
				return false;
			}
			
			@Override
			public boolean keyUp(int keycode) {
				
				return false;
			}
			
			@Override
			public boolean keyTyped(char character) {
				
				return false;
			}
			
			@Override
			public boolean keyDown(int keycode) {
				
				return false;
			}
		};
		
		Prev = new InputElement();
		Prev.setTexture(Helper.GetTexture("previous"));
		Prev.SizeFromImage();
		Prev.CenterY();
		Prev.MarginLeft(20);
		Prev.processor = new InputProcessor() {
			
			@Override
			public boolean touchUp(int screenX, int screenY, int pointer, int button) {
				return false;
			}
			
			@Override
			public boolean touchDragged(int screenX, int screenY, int pointer) {
				return false;
			}
			
			@Override
			public boolean touchDown(int screenX, int screenY, int pointer, int button) {
				if(WorldIndex > 0){
					WorldIndex--;
				}
				return false;
			}
			
			@Override
			public boolean scrolled(int amount) {
				return false;
			}
			
			@Override
			public boolean mouseMoved(int screenX, int screenY) {
				return false;
			}
			
			@Override
			public boolean keyUp(int keycode) {
				return false;
			}
			
			@Override
			public boolean keyTyped(char character) {
				return false;
			}
			
			@Override
			public boolean keyDown(int keycode) {
				return false;
			}
		};
		
		StartGame = new InputElement();
		StartGame.setTexture(Helper.GetTexture("startgame"));
		StartGame.SizeFromImage();
		StartGame.CenterX();
		StartGame.MarginBottom(20);
		StartGame.processor = new InputProcessor() {
			
			@Override
			public boolean touchUp(int screenX, int screenY, int pointer, int button) {
				return false;
			}
			
			@Override
			public boolean touchDragged(int screenX, int screenY, int pointer) {
				return false;
			}
			
			@Override
			public boolean touchDown(int screenX, int screenY, int pointer, int button) {
				World.OFFSET_X = 0;
				World.OFFSET_Y = 0;
				World.setScale(1f);
				World.LoadMap(maps.get(WorldIndex));
				World.setScale(GridTD2.isMobile ? 3.5f : 1f);
				SceneManager.setScene(World.getInstance());
				return false;
			}
			
			@Override
			public boolean scrolled(int amount) {
				return false;
			}
			
			@Override
			public boolean mouseMoved(int screenX, int screenY) {
				return false;
			}
			
			@Override
			public boolean keyUp(int keycode) {
				return false;
			}
			
			@Override
			public boolean keyTyped(char character) {
				return false;
			}
			
			@Override
			public boolean keyDown(int keycode) {
				return false;
			}
		};
		
		AddElement(Next);
		AddElement(Prev);
		AddElement(StartGame);
	}
	
	private void InitWorlds() {
		for(int i = 0; i < maps.size(); i++) {
			maps.get(i).Init();
		}
	}
	
	@Override
	public void Render(ShapeRenderer shape, SpriteBatch sprite) {
		if(maps.size() <= 0) {
			SceneManager.setScene(new StartMenu());
			return;
		}
		sprite.end();
		shape.setColor(0.14f, 0.14f, 0.14f, 1f);
		shape.begin(ShapeType.Filled);
		
		shape.rect(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		
		shape.end();
		sprite.begin();
		Map map = maps.get(WorldIndex);
		map.Render(sprite, shape);
		
		if(map.mapJson != null && map.mapJson.Name != null) {
			TextBounds bounds = Helper.GetFont().getBounds(map.mapJson.Name);
			if(bounds != null) {
				Helper.GetFont().draw(sprite, map.mapJson.Name, Gdx.graphics.getWidth() / 2 - bounds.width / 2, Gdx.graphics.getHeight() - bounds.height);
			}
		}
		
		if(WorldIndex > 0) {
			Prev.setVisible(true);
		}else {
			Prev.setVisible(false);
		}
		if(WorldIndex + 1 < maps.size()) {
			Next.setVisible(true);
		}else {
			Next.setVisible(false);
		}
		
		super.Render(shape, sprite);
	}
	
}
