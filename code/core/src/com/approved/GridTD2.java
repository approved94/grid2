package com.approved;

import com.approved.StartMenu.StartMenu;
import com.approved.input.InputManager;
import com.approved.player.Player;
import com.approved.scenes.SceneManager;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class GridTD2 extends Game implements InputProcessor {
	private SpriteBatch sprite;
	private ShapeRenderer shape;
	public static boolean isMobile;
	
	public GridTD2(boolean isMobile) {
		super();
		GridTD2.isMobile = isMobile;
	}

	@Override
	public void create() {
		sprite = new SpriteBatch();
		shape = new ShapeRenderer();
		Gdx.input.setInputProcessor(InputManager.getInstance());
		InputManager.AddListener(this);
		
		SceneManager.setScene(new StartMenu());
	}
	
	@Override
	public void dispose() {
		Player.UploadToServer();
		super.dispose();
		sprite.dispose();
		shape.dispose();
	}
	
	@Override
	public void pause() {
		Player.UploadToServer();
		super.pause();
	}
	
	@Override
	public void render() {
		sprite.begin();
		// Clear screen for for next frame
		Gdx.gl.glClearColor(0, 0, 0, 0);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
		SceneManager.Render(shape, sprite); // Render scene
		sprite.end();
	}

	@Override
	public boolean keyDown(int keycode) {
		
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		if(keycode == 248) { // F5
			//World.Init();
			SceneManager.setScene(new StartMenu());
			System.out.println("Returned to startmenu by keypress on F5");
		}
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}
	
	
}
