package com.approved;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;

import com.approved.player.Player;
import com.approved.world.Map;

public class Server {
	private static String baseURL = "http://approved.comlu.com/Grid/";
	
	public static String GetMap(int id) {
		return GetResponse("Map.php?method=get_map&id=" + id);
	}
	
	public static String GetResponse(String args) {
		String r = "";
		try {
			URL url = new URL(baseURL + args);
			URLConnection con = url.openConnection();
			
			BufferedReader rd = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String line = "";
			while ((line = rd.readLine()) != null) {
				if(line.contains("<")) break;
				r += line;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return r;
	}

	public static void UploadMap(String json) {
		try {
			GetResponse("Map.php?method=upload_map&json=" + URLEncoder.encode(json, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
	}

	public static ArrayList<Map> GetMaps() {
		ArrayList<Map> r = new ArrayList<Map>();
		try {
			URL url = new URL(baseURL + "Map.php?method=get_maps");
			URLConnection con = url.openConnection();
			
			BufferedReader rd = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String line = "";
			while ((line = rd.readLine()) != null) {
				String[] split = line.split("<br>");
				for(String s : split) {
					r.add(Map.FromJson(s));
				}
				if(line.contains("<")) break;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return r;
	}
	
	public static String GetPlayer(String uid) {
		try {
			return GetResponse("Player.php?method=GetPlayer&uid=" + URLEncoder.encode(uid, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return "";
	}

	public static void UpdatePlayer(String json) {
		try {
			GetResponse("Player.php?method=UpdatePlayer&uid=" + URLEncoder.encode(Player.GetInstance().uid, "UTF-8") + "&json=" + URLEncoder.encode(json, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
}
