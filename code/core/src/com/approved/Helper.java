package com.approved;

import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Helper {
	private static HashMap<String, TextureRegion> TEXTURES = new HashMap<String, TextureRegion>();
	private static TextureAtlas ATLAS;
	private static Texture t;
	private static BitmapFont font;
	
	public static TextureRegion GetTexture(String name) {
		if(TEXTURES.containsKey(name.toLowerCase())) {
			return TEXTURES.get(name.toLowerCase());
		}else {
			if(ATLAS == null) ATLAS = new TextureAtlas(Gdx.files.internal("GridTDTextures.atlas"));
			if(t == null) t = new Texture(Gdx.files.internal("GridTDTextures.png"));
			TextureRegion tex = GetTextureRegion(name);
			TEXTURES.put(name.toLowerCase(), tex);
			return tex;
		}
	}
	
	public static TextureRegion GetTextureRegion(String Region){
		AtlasRegion reg = ATLAS.findRegion(Region);
		if(reg == null) {
			System.out.println("Coulnd't find texture: " + Region);
			return new TextureRegion(new Texture(Gdx.files.internal("badlogic.jpg"))); // In case we don't find texture, return dummy-image to prevent crashes
		}
		return new TextureRegion(t, reg.getRegionX(), reg.getRegionY(), reg.getRegionWidth(), reg.getRegionHeight());
	}
	
	public static int Random(int Min, int Max) {
		if(Max < Min) return 0; 
		return (int) (Min + (int)(Math.random() * ((Max - Min) + 1)));
	}
	
	public static void EnableBlend() {
		Gdx.gl.glEnable(GL20.GL_BLEND);
	    Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
	}
	
	public static void DisableBlend() {
		Gdx.gl.glDisable(GL20.GL_BLEND);
	}
	
	public static BitmapFont GetFont() {
		if(font == null) {
			font = new BitmapFont(Gdx.files.internal("test.fnt"));
			font.setColor(1, 1, 1, 1); // This is white
		}
		return font;
	}

	public static void ClearTextures() {
		TEXTURES.clear();
		TEXTURES = null;
		TEXTURES = new HashMap<String, TextureRegion>();
		
		if(ATLAS != null)ATLAS.dispose();
		ATLAS = null;
		if(t != null) t.dispose();
		t = null;
		
		if(font != null) font.dispose();
		font = null;
	}
	
}
