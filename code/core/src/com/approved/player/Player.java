package com.approved.player;

import com.approved.Server;
import com.badlogic.gdx.utils.Json;

public class Player {
	public static float Money = 100;
	public static int Health = 100;
	private static Player instance;
	
	public String uid;
	public int xp;
	public int gold;
	
	public static Player GetInstance() {
		if(instance == null) instance = new Player();
		return instance;
	}
	
	public String ToJson() {
		Json json = new Json();
		return json.toJson(this);
	}
	
	public static void LoadFromJson(String uid) {
		Json json = new Json();
		String re = Server.GetPlayer(uid);
		System.out.println("DATA REPONSE: " + re);
		instance = json.fromJson(Player.class, re);
	}
	
	public static void UploadToServer() {
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				Server.UpdatePlayer(GetInstance().ToJson());
			}
		}).start();
	}
	
	public static void ApplyHealth(int amount) {
		Health += amount;
	}
	
	public static void ApplyMoney(float amount) {
		Money += amount;
	}
}
