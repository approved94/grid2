package com.approved.input;

import java.util.HashMap;

import com.badlogic.gdx.InputProcessor;

public abstract class InputProcessorWTags implements InputProcessor {
	public HashMap<String, Object> tags = new HashMap<String, Object>();
	
	
	public void AddTag(String name, Object tag) {
		tags.put(name, tag);
	}
	
	public Object GetTag(String name) {
		if(tags.containsKey(name)) {
			return tags.get(name);
		}
		return null;
	}
}
