package com.approved.input;

import com.approved.Helper;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class InputButton extends InputElement {

	public String Text = "";
	@Override
	public void render(ShapeRenderer shape, SpriteBatch sprite) {
		super.render(shape, sprite);
		TextBounds bounds = Helper.GetFont().getBounds(Text);
		Helper.GetFont().draw(sprite, Text, getX(), getY()+bounds.height);
	}
	public void SizeFromText() {
		TextBounds bounds = Helper.GetFont().getBounds(Text);
		setWidth(bounds.width);
		setHeight(bounds.height);
	}
	
}
