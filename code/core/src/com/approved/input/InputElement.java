package com.approved.input;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Scaling;

public class InputElement implements InputProcessor {
	private float X, Y, Width, Height;
	public InputProcessor processor;
	private TextureRegion Texture;
	private float a=1,r=1,g=1,b=1;
	private boolean Visible = true;
	
	@Override
	public boolean keyDown(int keycode) {
		if(processor != null) return processor.keyDown(keycode);
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		if(processor != null) return processor.keyUp(keycode);
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		if(processor != null) return processor.keyTyped(character);
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		if(processor != null) return processor.touchDown(screenX, screenY, pointer, button);
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		if(processor != null) return processor.touchUp(screenX, screenY, pointer, button);
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		if(processor != null) return processor.touchDragged(screenX, screenY, pointer);
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		if(processor != null) return processor.mouseMoved(screenX, screenY);
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		if(processor != null) return processor.scrolled(amount);
		return false;
	}

	public void render(ShapeRenderer shape, SpriteBatch sprite) {
		if(!isVisible()) return;
		sprite.setColor(r, g, b, a);
		if(getTexture() != null) {
			sprite.draw(getTexture(), getX(), getY(), getWidth(), getHeight());
		}
		sprite.setColor(Color.WHITE);
	}

	public float getX() {
		return X;
	}

	public void setX(float x) {
		X = x;
	}

	public float getY() {
		return Y;
	}

	public void setY(float y) {
		Y = y;
	}

	public float getWidth() {
		return Width;
	}

	public void setWidth(float width) {
		Width = width;
	}
	
	public void setWidthPercentage(float percent) {
		setWidth((Gdx.graphics.getWidth()*(percent / 100f)));
	}

	public float getHeight() {
		return Height;
	}

	public void setHeight(float height) {
		Height = height;
	}
	
	public void setHeightPercentage(float percent) {
		setHeight((Gdx.graphics.getHeight() * (percent / 100f)));
	}
	
	public void CenterX() {
		setX(Gdx.graphics.getWidth() / 2 - getWidth() / 2);
	}
	
	public void CenterY() {
		setY(Gdx.graphics.getHeight() / 2 - getHeight() / 2);
	}
	
	public void Center() 
	{
		CenterX();
		CenterY();
	}
	
	public void MarginTop(float margin) {
		setY(Gdx.graphics.getHeight() - getHeight() -margin);
	}
	
	public void MarginTopPercentage(float percent) {
		float rootPix = (Gdx.graphics.getHeight() - getHeight());
		float removePix = (rootPix * (percent / 100)); 
		setY(rootPix - removePix);
	}
	
	public boolean Within(float x, float y) {
		if(!isVisible()) return false;
		if(x >= getX() && x <= getX() + getWidth()) {
			if(y >= getY() && y <= getY() + getHeight()) return true;
		}
		return false;
	}
	
	public boolean Within(float x, float y, boolean FlipY) {
		return Within(x, FlipY ? (Gdx.graphics.getHeight() - y) : y);
	}
	
	public void Scale(float percent) {
		setWidth(getTexture().getRegionWidth()*percent);
		setHeight(getTexture().getRegionHeight()*percent);
		
		if(getWidth() > Gdx.graphics.getWidth()) setWidth(Gdx.graphics.getWidth());
		if(getHeight() > Gdx.graphics.getHeight()) setWidth(Gdx.graphics.getHeight());
	}

	public TextureRegion getTexture() {
		return Texture;
	}

	public void setTexture(TextureRegion texture) {
		Texture = texture;
	}
	
	public void SizeFromImage() {
		if(getTexture() == null) return;
		setWidth(getTexture().getRegionWidth());
		setHeight(getTexture().getRegionHeight());
	}

	public void MarginRight(float margin) {
		setX(Gdx.graphics.getWidth() - getWidth() - margin);
	}
	
	public void MarginBottom(float margin) {
		setY(margin);
	}
	
	public void SetColor(float r, float g, float b, float a) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}

	public void MarginLeft(float margin) {
		setX(margin);
	}

	public boolean isVisible() {
		return Visible;
	}

	public void setVisible(boolean visible) {
		Visible = visible;
	}
	
	public void SetWidthScreen(float percentage) {
		float ratio = (float)getTexture().getRegionWidth() / (float)getTexture().getRegionHeight();
		setWidthPercentage(percentage);
		setHeight(getWidth()/ratio);
	}

}
