package com.approved.input;

import java.util.ArrayList;

import com.badlogic.gdx.InputProcessor;

public class InputManager implements InputProcessor {
	private static InputManager INSTANCE;
	private static ArrayList<InputProcessor> LISTENERS = new ArrayList<InputProcessor>();

	public static InputManager getInstance() {
		if (INSTANCE == null) INSTANCE = new InputManager();
		return INSTANCE;
	}
	
	public static void AddListener(InputProcessor listener) {
		if(LISTENERS.contains(listener)) return;
		LISTENERS.add(listener);
	}
	
	public static void RemoveListener(InputProcessor listener) {
		while(LISTENERS.contains(listener)) {
			LISTENERS.remove(listener);
		}
	}

	@Override
	public boolean keyDown(int keycode) {
		for(int i = 0; i < LISTENERS.size(); i++) {
			if(LISTENERS.get(i).keyDown(keycode)) return true;
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		for(int i = 0; i < LISTENERS.size(); i++) {
			if(LISTENERS.get(i).keyUp(keycode)) return true;
		}
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		for(int i = 0; i < LISTENERS.size(); i++) {
			if(LISTENERS.get(i).keyTyped(character)) return true;
		}
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		for(int i = 0; i < LISTENERS.size(); i++) {
			LISTENERS.get(i).touchDown(screenX, screenY, pointer, button);
		}
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		for(int i = 0; i < LISTENERS.size(); i++) {
			LISTENERS.get(i).touchUp(screenX, screenY, pointer, button);
		}
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		for(int i = 0; i < LISTENERS.size(); i++) {
			LISTENERS.get(i).touchDragged(screenX, screenY, pointer);
		}
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		for(int i = 0; i < LISTENERS.size(); i++) {
			if(LISTENERS.get(i).mouseMoved(screenX, screenY)) return true;
		}
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		for(int i = 0; i < LISTENERS.size(); i++) {
			if(LISTENERS.get(i).scrolled(amount)) return true;
		}
		return false;
	}

}
