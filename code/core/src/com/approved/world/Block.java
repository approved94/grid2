package com.approved.world;

import com.approved.Point;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class Block {
	private float X,Y,Width,Height;
	private TextureRegion Background, Texture;
	private static int SIZE = 32;
	public static int SIZE_ORG = 32;
	public boolean IS_ROAD, IS_TURRET;
	
	public Block(float f, float g) {
		Width = SIZE;
		Height = SIZE;
		X = f;
		Y = g;
	}

	public TextureRegion getBackground() {
		return Background;
	}

	public void setBackground(TextureRegion background) {
		Background = background;
	}

	public float getX() {
		return X;
	}
	
	public float getRenderX() {
		return (getX() - World.OFFSET_X) * World.getScale();
	}

	public void setX(float x) {
		X = x;
	}

	public float getY() {
		return Y;
	}
	
	public float getRenderY() {
		return (getY() - World.OFFSET_Y) * World.getScale();
	}

	public void setY(float y) {
		Y = y;
	}

	public float getWidth() {
		return Width;
	}
	
	public float getRenderWidth() {
		return getWidth() * World.getScale();
	}

	public void setWidth(float width) {
		Width = width;
	}

	public float getHeight() {
		return Height;
	}
	
	public float getRenderHeight() {
		return getHeight() * World.getScale();
	}

	public void setHeight(float height) {
		Height = height;
	}
	
	public void SetPosition(float x, float y) {
		setX(x);
		setY(y);
	}
	
	
	/**
	 * Will render the block fully
	 */
	public void RenderFully(ShapeRenderer shape, SpriteBatch sprite) {
		if(Background != null) sprite.draw(Background, getRenderX(), getRenderY(), getRenderWidth(), getRenderHeight());
		if(Texture != null) sprite.draw(getTexture(), getRenderX(), getRenderY(), getRenderWidth(), getRenderHeight());
		Render(shape, sprite);
	}

	/**
	 * will be overridden in other classes
	 */
	public void Render(ShapeRenderer shape, SpriteBatch sprite) {
		
	}
	
	/**
	 * Is the given cordinates inside this block? Vissably in this block
	 * @param x2 The pixel X
	 * @param y2 The pixel Y
	 * @return
	 */
	public boolean Inside(float x, float y) {
		if(x >= this.getRenderX() && x <= this.getRenderX() + Block.GetSize()) {
			if(y >= this.getRenderY() && y <= this.getRenderY() + Block.GetSize()) return true;
		}
		return false;
	}
	
	public void Tick() {

	}
	
	public Point MiddlePoint() {
		return new Point(getX() + Block.SIZE / 2, getY() + Block.SIZE / 2);
	}
	
	public int MiddleX() {
		return MiddlePoint().x;
	}
	
	public float MiddleRenderX() {
		return getRenderX() + Block.GetSize() / 2; 
	}
	
	public int MiddleY() 
	{
		return MiddlePoint().y;
	}
	
	public float MiddleRenderY() {
		return getRenderY() + Block.GetSize() / 2;
	}

	public TextureRegion getTexture() {
		return Texture;
	}

	public void setTexture(TextureRegion texture) {
		Texture = texture;
	}
	
	public Block Up() {
		return World.BlockAtPix(getX(), getY() + Block.SIZE + Block.SIZE / 2);
	}
	
	public Block Down() {
		return World.BlockAtPix(getX(), getY() - Block.SIZE / 2);
	}
	
	public Block Left() {
		return World.BlockAtPix(getX() - Block.SIZE / 2, getY());
	}
	
	public Block Right() {
		return World.BlockAtPix(getX() + Block.SIZE / 2, getY());
	}
	
	public static int GetSize() {
		return (int)(Block.SIZE * World.getScale());
	}
	
	public static void SetSize(int size) {
		Block.SIZE = size;
	}

	public static int GetBaseSize() {
		return Block.SIZE;
	}
	
	public int GetBlockX() {
		return (int) (getX() / Block.GetBaseSize());
	}
	
	public int GetBlockY() {
		return (int) (getY() / Block.GetBaseSize());
	}
}
