package com.approved.world.turrets;

import com.approved.Helper;
import com.approved.world.Block;
import com.approved.world.mobs.BaseMob;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

public class LaserTurret extends BaseTurret {
	private BaseMob Attacking = null;
	private float degrees = 0;
	private Sprite s;

	public LaserTurret(float f, float g) {
		super(f, g);
		degrees = Helper.Random(0, 380);
		setTexture(Helper.GetTexture("laseridle"));
		args.Range = Block.GetBaseSize() * 2; // Five blocks range
		args.BuyCost = 72;
		args.MoneyGained -= args.BuyCost;
		args.Select = Helper.GetTexture("choosing-laserturret");
		setDamage(4);
		
		upgrades.add(args);
		
		TurretArgs upgrade = new TurretArgs();
		upgrade.Range = args.Range * 3;
		upgrade.BuyCost = 300;
		upgrades.add(upgrade);
		
		TurretArgs upgrade1 = new TurretArgs();
		upgrade1.Damage = args.Damage * 5;
		upgrade1.BuyCost = 500;
		upgrades.add(upgrade1);
	}

	@Override
	public void Attack() {
		if (Mobs.size() <= 0) {
			setTexture(Helper.GetTexture("laseridle"));
			Attacking = null;
			return;
		}
		
		BaseMob mob = Attacking;
		if(mob == null) mob = Mobs.get(0);
		if(mob.PixelsAway(this)>args.Range || mob == null) {
			float pixAway = mob.PixelsAway(this);
			for(int i = 0; i < Mobs.size(); i++) {
				if(Mobs.get(i).PixelsAway(this) < pixAway) {
					mob = Mobs.get(i);
					pixAway = mob.PixelsAway(this);
				}
			}
		}
		
		
		mob.Damage(getDamage());
		if (mob.isDead()) {
			setTexture(Helper.GetTexture("laseridle"));
			args.Kills++;
			args.MoneyGained += mob.getWorth();
			Attacking = null;
		} else {
			setTexture(Helper.GetTexture("lasershooting"));
			Attacking = mob;
		}
	}

	@Override
	public void RenderFully(ShapeRenderer shape, SpriteBatch sprite) {
		if(getBackground() != null) sprite.draw(getBackground(), getRenderX(), getRenderY(), getWidth(), getHeight());
		Render(shape, sprite);
	}
	
	@Override
	public void Render(ShapeRenderer shape, SpriteBatch sprite) {
		if(Attacking != null) degrees = (float) ((Math.atan2 (Attacking.MiddleX() - MiddleX(), - (Attacking.MiddleY() - MiddleY()))*180.0d/Math.PI) + 180.0f);
		
		getTexture().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		if(s == null) s = new Sprite(getTexture());
		s.setRotation(degrees);
		s.setOrigin(getRenderWidth() / 2, getRenderHeight() / 2);
		s.setSize(getRenderWidth(), getRenderHeight());
		s.setX(getRenderX());
		s.setY(getRenderY());
		s.draw(sprite);
	}
	
	@Override
	public BaseTurret New(float x, float y) {
		return new LaserTurret(x, y);
	}
	
	@Override
	public void RenderEffects(ShapeRenderer shape, SpriteBatch sprite) {
		if(Attacking != null) {
			sprite.end();
			shape.begin(ShapeType.Line);
			shape.setColor(1, 0, 0, 1);
			shape.line(MiddleRenderX(), MiddleRenderY(), Attacking.MiddleRenderX(), Attacking.MiddleRenderY());
			shape.end();
			sprite.begin();
		}
	}

}
