package com.approved.world.turrets;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class TurretArgs {
	public String Description = "";
	public float Damage;
	public int Kills = 0;
	public float MoneyGained = 0f;
	public float Range = 0;
	public float BuyCost = 0;
	public ArrayList<TurretArgs> Upgrades = new ArrayList<TurretArgs>();
	public TextureRegion Select;
	
	public void ApplyUpgrade(TurretArgs upgrade) {
		if(upgrade.Damage != 0) Damage = upgrade.Damage;
		if(upgrade.Range != 0) Range = upgrade.Range;
		
		MoneyGained -= upgrade.BuyCost;
	}
	
}
