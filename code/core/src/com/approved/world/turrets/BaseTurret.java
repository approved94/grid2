package com.approved.world.turrets;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import com.approved.Helper;
import com.approved.Point;
import com.approved.input.InputButton;
import com.approved.input.InputManager;
import com.approved.input.InputProcessorWTags;
import com.approved.player.Player;
import com.approved.world.Block;
import com.approved.world.World;
import com.approved.world.mobs.BaseMob;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

public class BaseTurret extends Block implements InputProcessor {
	public float TickDelay = 1000/20;
	public long LastTick;
	public TurretArgs args = new TurretArgs();
	protected ArrayList<BaseMob> Mobs;
	public int CurrentTier = 0;
	public ArrayList<TurretArgs> upgrades = new ArrayList<TurretArgs>();
	public InputButton UpgradeButton = null;
	
	public BaseTurret(float f, float g) {
		super(f, g);
		UpgradeButton = new InputButton();
		UpgradeButton.Text = "Upgrade";
		UpgradeButton.SizeFromText();
		UpgradeButton.setX(Gdx.graphics.getWidth() - World.TurretMenuWidth + (World.TurretMenuWidth / 2 - UpgradeButton.getWidth() / 2));
		UpgradeButton.setY(Gdx.graphics.getHeight() * 0.45f);
		UpgradeButton.processor = new InputProcessorWTags() {
			
			@Override
			public boolean touchUp(int screenX, int screenY, int pointer, int button) {
				if(UpgradeButton.Within(screenX, screenY, true)) {
					System.out.println("TEST");
					((BaseTurret)(((InputProcessorWTags)this).GetTag("turret"))).Upgrade();
				}
				return false;
			}
			
			@Override
			public boolean touchDragged(int screenX, int screenY, int pointer) {
				return false;
			}
			
			@Override
			public boolean touchDown(int screenX, int screenY, int pointer, int button) {
				return false;
			}
			
			@Override
			public boolean scrolled(int amount) {
				return false;
			}
			
			@Override
			public boolean mouseMoved(int screenX, int screenY) {
				return false;
			}
			
			@Override
			public boolean keyUp(int keycode) {
				
				return false;
			}
			
			@Override
			public boolean keyTyped(char character) {
				return false;
			}
			
			@Override
			public boolean keyDown(int keycode) {
				
				return false;
			}
		};
		((InputProcessorWTags)UpgradeButton.processor).AddTag("turret", this);
		args.MoneyGained -= args.BuyCost;
	}
	
	@Override
	public void Tick() {
		Mobs = GetMobs();
		Attack();
	}
	
	public void Attack() {
		
	}
	
	/**
	 * 
	 * @return All mobs that are in range of the turret
	 */
	public ArrayList<BaseMob> GetMobs() {
		ArrayList<BaseMob> mobs = new ArrayList<BaseMob>();
		
		for(int i = 0; i < World.GetMobs().size(); i++) {
			BaseMob mob = World.GetMobs().get(i);
			if(InRange(mob.MiddlePoint())) {
				mobs.add(mob);
			}
		}
		
		Collections.sort(mobs, new Comparator<BaseMob>() {

			@Override
			public int compare(BaseMob o1, BaseMob o2) {
				if(o2.getDistanceTraveled() > o1.getDistanceTraveled()) {
					return 1;
				}else if(o2.getDistanceTraveled() < o1.getDistanceTraveled()) {
					return -1;
				}
				return 0;
			}
		});
		
		return mobs;
	}
	
	public boolean InRange(Point p) {
		// Math.abs make the value positive no matter what, used here to calculate the diffrence betwen values
		int x = (int) Math.abs(MiddleX() - p.x);
		int y = (int) Math.abs(MiddleY() - p.y);
		if(x > args.Range || y > args.Range) return false; // Is any of the values calculated is beyond the range of this turret then return false, otherwise return true
		return true;
	}

	public float getDamage() {
		return args.Damage;
	}

	public void setDamage(int damage) {
		args.Damage = damage;
	}

	public BaseTurret New(float x, float y) {
		return new BaseTurret(x, y);
	}
	
	@Override
	public void RenderFully(ShapeRenderer shape, SpriteBatch sprite) {
		super.RenderFully(shape, sprite);
		RenderInfo(shape, sprite);
	}

	public void RenderEffects(ShapeRenderer shape, SpriteBatch sprite) {
		
	}
	
	public void RenderPlacement(ShapeRenderer shape, SpriteBatch sprite) {
		if(getTexture() == null) return;
		Block b = World.GetMouseHoverBlock();
		sprite.draw(getTexture(), b.getRenderX(), b.getRenderY(), Block.GetSize(), Block.GetSize());
		sprite.end();
		Helper.EnableBlend();
		shape.begin(ShapeType.Filled);
		if(CanPlace()) {
			shape.setColor(0, 1, 0, 0.3f);
		}else {
			shape.setColor(1, 0, 0, 0.3f);
		}
		float radius = args.Range;
		radius *= World.getScale();
		
		shape.circle(b.getRenderX()+Block.GetSize()/2, b.getRenderY() + Block.GetSize()/2, radius);
		shape.end();
		Helper.DisableBlend();
		shape.begin(ShapeType.Line);
		shape.setColor(0, 0, 0, 1);
		shape.circle(b.getRenderX()+Block.GetSize()/2, b.getRenderY() + Block.GetSize()/2, radius);
		shape.end();
		sprite.begin();
	}

	public boolean CanPlace() {
		Block b = World.GetMouseHoverBlock();
		return (!b.IS_ROAD && !b.IS_TURRET && Player.Money >= args.BuyCost);
	}

	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {		
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		if(Within(screenX + World.OFFSET_X, Gdx.graphics.getHeight() - screenY + World.OFFSET_Y)){
			World.SELECTED_TURRET = this;
		}
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}
	
	public void RenderInfo(ShapeRenderer shape, SpriteBatch sprite) {
		if(getX() < 0 || getY() < 0) return;
		if(World.SELECTED_TURRET == this) {
			sprite.end();
			
			shape.begin(ShapeType.Filled);
			shape.setColor(0,0,1,1); // Green
			shape.rect(Gdx.graphics.getWidth() - World.TurretMenuWidth, 0, World.TurretMenuWidth, Gdx.graphics.getHeight());
			shape.end();
			
			TurretArgs upgrade = NextUpgrade();
			if(upgrade != null) {
				// Draw upgrade range
				Helper.EnableBlend();
				shape.begin(ShapeType.Filled);
				shape.setColor(0,1,0,0.3f);
				
				shape.circle(getRenderX()+Block.GetSize()/2, getRenderY() + Block.GetSize()/2, upgrade.Range * World.getScale());
				shape.end();
				Helper.DisableBlend();
				shape.begin(ShapeType.Line);
				shape.setColor(0, 0, 0, 1);
				shape.circle(getRenderX()+Block.GetSize()/2, getRenderY() + Block.GetSize()/2, upgrade.Range * World.getScale());
				shape.end();
			}
			
			
			// Draw current range
			Helper.EnableBlend();
			shape.begin(ShapeType.Filled);
			shape.setColor(0,0,1,0.3f);
			
			shape.circle(getRenderX()+Block.GetSize()/2, getRenderY() + Block.GetSize()/2, args.Range * World.getScale());
			shape.end();
			Helper.DisableBlend();
			shape.begin(ShapeType.Line);
			shape.setColor(0, 0, 0, 1);
			shape.circle(getRenderX()+Block.GetSize()/2, getRenderY() + Block.GetSize()/2, args.Range * World.getScale());
			shape.end();
			sprite.begin();
			
			InputManager.AddListener(UpgradeButton);
			if(CanUpgrade()) {
				Helper.GetFont().setColor(0,1,0,1);
			}else {
				Helper.GetFont().setColor(1f / 163f, 1f / 163f, 1f / 163f, 1f);
			}
			UpgradeButton.render(shape, sprite);
			Helper.GetFont().setColor(1,1,1,1);
			
			float newDamage = args.Damage, cost = 0;
			if(upgrade != null){
				if(upgrade.Damage != 0) {
					newDamage = upgrade.Damage;
				}
				cost = upgrade.BuyCost;
			}
			TextBounds bounds = Helper.GetFont().getBounds("Damage: " + args.Damage + " -> " + newDamage);
			float x = Gdx.graphics.getWidth() - World.TurretMenuWidth + (World.TurretMenuWidth / 2 - bounds.width / 2);
			Helper.GetFont().draw(sprite, "Damage: " + args.Damage + " -> " + newDamage, x, Gdx.graphics.getHeight() * 0.95f);
			
			
			bounds = Helper.GetFont().getBounds("Cost: " + cost);
			x = Gdx.graphics.getWidth() - World.TurretMenuWidth + (World.TurretMenuWidth / 2 - bounds.width / 2);
			Helper.GetFont().draw(sprite, "Cost: " + cost, x, Gdx.graphics.getHeight() * 0.55f);
			
			
			// Lower part:
			bounds = Helper.GetFont().getBounds("Money gained: " + args.MoneyGained);
			x = Gdx.graphics.getWidth() - World.TurretMenuWidth + (World.TurretMenuWidth / 2 - bounds.width / 2);
			Helper.GetFont().draw(sprite, "Money gained: " + args.MoneyGained, x, Gdx.graphics.getHeight() * 0.11f);
			
			bounds = Helper.GetFont().getBounds("Kills: " + args.Kills);
			x = Gdx.graphics.getWidth() - World.TurretMenuWidth + (World.TurretMenuWidth / 2 - bounds.width / 2);
			Helper.GetFont().draw(sprite, "Kills: " + args.Kills, x, Gdx.graphics.getHeight() * 0.05f);
		}else {
			InputManager.RemoveListener(UpgradeButton);
		}
	}
	
	public boolean Within(float x, float y) {
		return Inside(x, y);
	}
	
	public TurretArgs NextUpgrade() {
		if(CurrentTier + 1 <= upgrades.size()-1) {
			return upgrades.get(CurrentTier + 1);
		}
		return null;
	}
	
	public boolean CanUpgrade() {
		TurretArgs upgrade = NextUpgrade();
		if(upgrade == null) return false;
		if(Player.Money >= upgrade.BuyCost) {
			return true;
		}
		return false;
	}
	
	public void Upgrade() {
		if(!CanUpgrade()) return;
		TurretArgs upgrade = NextUpgrade();
		Player.ApplyMoney(upgrade.BuyCost / -1);
		args.ApplyUpgrade(upgrade);
		CurrentTier++;
	}
}
