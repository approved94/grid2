package com.approved.world.turrets;

import com.approved.Helper;
import com.approved.world.Block;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class TeslaTower extends BaseTurret {

	public TeslaTower(float f, float g) {
		super(f, g);
		setDamage(2);
		args.Range = Block.GetBaseSize() * 1;
		args.BuyCost = 25;
		args.MoneyGained -= args.BuyCost;
		args.Select = Helper.GetTexture("choosing-teslatower");
		setTexture(Helper.GetTexture("teslaidle"));
		upgrades.add(args);
		
		TurretArgs upgrade = new TurretArgs();
		upgrade.BuyCost = 30000;
		upgrade.Damage = 25;
		upgrades.add(upgrade);
	}
	
	@Override
	public void Attack() {
		for(int i = 0; i < Mobs.size(); i++) {
			Mobs.get(i).Damage(getDamage() / Mobs.size());
			if(Mobs.get(i).isDead()) {
				args.Kills++;
				args.MoneyGained += Mobs.get(i).getWorth();
			}
		}
	}
	
	@Override
	public BaseTurret New(float x, float y) {
		return new TeslaTower(x, y);
	}
	
	@Override
	public void Render(ShapeRenderer shape, SpriteBatch sprite) {
		if(GetMobs().size() > 0) {
			setTexture(Helper.GetTexture("teslapowered"));
		}else {
			setTexture(Helper.GetTexture("teslaidle"));
		}
		super.Render(shape, sprite);
	}

}
