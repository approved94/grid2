package com.approved.world;

import com.approved.Helper;
import com.approved.world.mobs.BaseMob;
import com.approved.world.mobs.FlyerMob;
import com.approved.world.mobs.SlimeMob;


public class MobWave {
	public static int Wave = 1;
	public static int WavesSpawn = 0;
	public static long LastSpawn = 0;
	private static boolean Spawn = true;
	private static Thread SpawnThread;
	
	public static void Spawn() {
		if(SpawnThread == null) {
			SpawnThread = new Thread(new Runnable() {
				
				@Override
				public void run() {
					while(true) {
						if(!Spawn) break;
						if(WavesSpawn == Wave*2) {
							if(System.currentTimeMillis() - LastSpawn >= 15000) {
								Wave++;
								WavesSpawn = 0;
							}else {
								try {
									Thread.sleep((long)100);
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
							}
						}
						
						float SpawnDelay = 1000 - (Wave * 10);
						if(SpawnDelay < 200) SpawnDelay = 200;
						if(System.currentTimeMillis() - LastSpawn >= SpawnDelay) {
							int r = Helper.Random(0, 1);
							BaseMob mob = null;
							if(r == 0) mob = new SlimeMob();
							if(r == 1) mob = new FlyerMob();
							
							int HP = (int) (mob.getHealth() * ((1000 / SpawnDelay)*1.5f));
							mob.setBaseHealth(HP);
							World.AddMob(mob);
							
							WavesSpawn++;
							LastSpawn = System.currentTimeMillis();
						}
					}
				}
			});
		}
		Spawn = true;
		if(!SpawnThread.isAlive()) {
			SpawnThread.start();
		}
	}
	
	public static void StopSpawning() {
		Spawn = false;
	}
	
}
