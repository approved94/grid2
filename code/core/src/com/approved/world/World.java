package com.approved.world;

import java.util.ArrayList;

import com.approved.Helper;
import com.approved.Point;
import com.approved.input.InputElement;
import com.approved.input.InputProcessorWTags;
import com.approved.player.Player;
import com.approved.scenes.Scene;
import com.approved.world.mobs.BaseMob;
import com.approved.world.mobs.FlyerMob;
import com.approved.world.mobs.SlimeMob;
import com.approved.world.turrets.BaseTurret;
import com.approved.world.turrets.LaserTurret;
import com.approved.world.turrets.TeslaTower;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector3;

public class World extends Scene {
	public static int OFFSET_X, OFFSET_Y;
	private static World INSTANCE;
	public static int BLOCKS_WIDE, BLOCKS_HIGH;
	private static long LAST_TICK = 0;
	public static int MOB_SPAWN_X, MOB_SPAWN_Y;
	private static ArrayList<BaseMob> MOBS = new ArrayList<BaseMob>();
	private static ArrayList<BaseTurret> TURRETS = new ArrayList<BaseTurret>();
	private static InputElement TurretButton;
	public static boolean TurretMenuOpen = false;
	public static BaseTurret MovingTurret;
	private static ArrayList<InputElement> SELECT_TOWER = new ArrayList<InputElement>();
	private static float Scale = 1;
	public static int TurretMenuWidth = 150;
	public static BaseTurret SELECTED_TURRET;
	public static Map MAP;
	private static Thread TickThread;

	// for pinch-to-zoom
	int numberOfFingers = 0;
	int fingerOnePointer;
	int fingerTwoPointer;
	float lastDistance = 0;
	Vector3 fingerOne = new Vector3();
	Vector3 fingerTwo = new Vector3();

	private static BaseTurret turret;

	public static void Init() {
		World.getInstance(); // Just to register it
		if (MAP == null)
			MAP = new Map();
		MAP.Init();

		TurretMenuWidth = (int) (Gdx.graphics.getWidth() * 0.35F);
		ArrayList<BaseTurret> RenderTurrets = new ArrayList<BaseTurret>();
		RenderTurrets.add(new LaserTurret(-10, -10));
		RenderTurrets.add(new TeslaTower(-10, -10));

		int lastY = Gdx.graphics.getHeight();
		int MarginTop = 25;
		for (int i = 0; i < RenderTurrets.size(); i++) {
			BaseTurret turret = RenderTurrets.get(i);
			InputElement element = new InputElement();
			element.setTexture(turret.args.Select);
			element.setWidthPercentage(8);
			element.setHeight(element.getWidth());

			int y = (int) (lastY - element.getHeight() - MarginTop);

			element.setX(Gdx.graphics.getWidth() - element.getWidth() / 2
					- TurretMenuWidth / 2);
			element.setY(y);

			element.processor = new InputProcessorWTags() {

				@Override
				public boolean touchUp(int screenX, int screenY, int pointer,
						int button) {

					return false;
				}

				@Override
				public boolean touchDragged(int screenX, int screenY,
						int pointer) {

					return false;
				}

				@Override
				public boolean touchDown(int screenX, int screenY, int pointer,
						int button) {
					World.turret = (BaseTurret) ((InputProcessorWTags) this)
							.GetTag("turret");
					World.CloseTurretMenu();
					return false;
				}

				@Override
				public boolean scrolled(int amount) {

					return false;
				}

				@Override
				public boolean mouseMoved(int screenX, int screenY) {

					return false;
				}

				@Override
				public boolean keyUp(int keycode) {

					return false;
				}

				@Override
				public boolean keyTyped(char character) {

					return false;
				}

				@Override
				public boolean keyDown(int keycode) {

					return false;
				}
			};
			((InputProcessorWTags) element.processor).AddTag("turret", turret);
			SELECT_TOWER.add(element);

			lastY = y;
		}
	}

	public static void LoadMap(Map map) {
		if (map.mapJson.Waypoints == null || map.mapJson.Waypoints.size() <= 0) {
			// This is a empty map, stop spawning enimies
			MobWave.StopSpawning();
			for (int i = 0; i < MOBS.size(); i++) {
				try {
					BaseMob mob = MOBS.get(i);
					mob.Damage(mob.getHealth() + 100); // Kill it
					MOBS.remove(i);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
		MAP = map;
		if (map.mapJson == null)
			map.mapJson = new MapJson();
		World.Init();

		if (map.mapJson.Waypoints.size() > 0) {
			Block b = MAP.BlockAt(MAP.mapJson.Waypoints.get(0).x,
					MAP.mapJson.Waypoints.get(0).y);
			MOB_SPAWN_X = (int) b.getX();
			MOB_SPAWN_Y = (int) b.getY();

			MobWave.Spawn();
		}
	}

	public static void AddRoadPoint(int x, int y) {
		MAP.AddRoadPoint(x, y);
	}

	public static World getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new World();
			TurretButton = new InputElement();
			TurretButton.setTexture(Helper.GetTexture("choosing-laserturret"));
			TurretButton.setWidthPercentage(5);
			TurretButton.setHeight(TurretButton.getWidth());
			TurretButton.MarginRight(10);
			TurretButton.MarginBottom(10);

			TurretButton.processor = new InputProcessor() {
				private boolean MouseDownHere = false;

				@Override
				public boolean touchUp(int screenX, int screenY, int pointer,
						int button) {
					if (MouseDownHere) {
						World.OpenTurretMenu();
					}
					return false;
				}

				@Override
				public boolean touchDragged(int screenX, int screenY,
						int pointer) {

					return false;
				}

				@Override
				public boolean touchDown(int screenX, int screenY, int pointer,
						int button) {
					MouseDownHere = true;
					return false;
				}

				@Override
				public boolean scrolled(int amount) {

					return false;
				}

				@Override
				public boolean mouseMoved(int screenX, int screenY) {

					return false;
				}

				@Override
				public boolean keyUp(int keycode) {

					return false;
				}

				@Override
				public boolean keyTyped(char character) {

					return false;
				}

				@Override
				public boolean keyDown(int keycode) {

					return false;
				}
			};

			World.getInstance().AddElement(TurretButton);
		}
		return INSTANCE;
	}

	public static void OpenTurretMenu() {
		TurretMenuOpen = true;
		for (int i = 0; i < World.SELECT_TOWER.size(); i++) {
			World.getInstance().AddElement(SELECT_TOWER.get(i));
		}
	}

	public static void CloseTurretMenu() {
		TurretMenuOpen = false;
		boolean run = true;
		while (run) {
			for (int i = 0; i < World.SELECT_TOWER.size(); i++) {
				World.getInstance().RemoveElement(SELECT_TOWER.get(i));
			}
			run = false;
			for (int i = 0; i < World.getInstance().GetElements().size(); i++) {
				if (World.getInstance().GetElements().get(i) == World.SELECT_TOWER
						.get(0))
					run = true;
			}
		}
		World.getInstance().AddElement(TurretButton);
	}

	protected static void RenderTurretMenu(ShapeRenderer shape,
			SpriteBatch sprite) {
		if (TurretMenuOpen) {
			World.getInstance().RemoveElement(TurretButton);
			sprite.end();
			shape.begin(ShapeType.Filled);
			shape.setColor(0, 1, 0, 1); // Green
			shape.rect(Gdx.graphics.getWidth() - TurretMenuWidth, 0,
					TurretMenuWidth, Gdx.graphics.getHeight());
			shape.end();
			sprite.begin();

		}
	}

	public static void AddBlock(Block block) {
		World.MAP.BLOCKS.add(block);
	}

	public static void AddMob(BaseMob mob) {
		MOBS.add(mob);
	}

	public static void RemoveMob(BaseMob mob) {
		MOBS.remove(mob);
	}

	public static void AddTurret(BaseTurret turret) {
		for (int i = 0; i < TURRETS.size(); i++) {
			if (TURRETS.get(i).getX() == turret.getX()
					&& TURRETS.get(i).getY() == turret.getY()) {
				return;
			}
		}

		if (BlockAtPix(turret.getRenderX() + 1, turret.getRenderY() + 1).IS_ROAD)
			return;

		if (Player.Money >= turret.args.BuyCost) {
			Player.ApplyMoney(turret.args.BuyCost / -1);
			BlockAtPix(turret.getRenderX() + 1, turret.getRenderY() + 1).IS_TURRET = true;
			TURRETS.add(turret);
		}
	}

	public static void RemoveTurret(BaseTurret turret) {
		TURRETS.remove(turret);
	}

	public static void render(SpriteBatch sprite, ShapeRenderer shape) {
		RenderBackground(shape, sprite);

		// Render the mobs after world blocks
		for (int i = 0; i < MOBS.size(); i++) {
			World.MOBS.get(i).RenderFully(shape, sprite);
		}

		for (int i = 0; i < TURRETS.size(); i++) {
			World.TURRETS.get(i).RenderFully(shape, sprite);
		}

		for (int i = 0; i < TURRETS.size(); i++) {
			World.TURRETS.get(i).RenderEffects(shape, sprite);
		}

		for (int i = 0; i < MOBS.size(); i++) {
			World.MOBS.get(i).DrawHealth(shape, sprite);
		}

		for (int i = 0; i < TURRETS.size(); i++) {
			World.TURRETS.get(i).RenderInfo(shape, sprite);
		}
		Tick();

		// new LaserTurret(0, 0).RenderPlacement(shape, sprite);

		if (SELECTED_TURRET != null) {
			World.getInstance().RemoveElement(TurretButton);
		} else {
			World.getInstance().AddElement(TurretButton);
		}
		RenderInfo(sprite);
		// RenderDebugInfo(sprite);
		RenderTurretMenu(shape, sprite);

		if (turret != null) {
			turret.RenderPlacement(shape, sprite);
		}
	}

	public static void RenderBackground(ShapeRenderer shape, SpriteBatch sprite) {
		for (int i = 0; i < MAP.BLOCKS.size(); i++) {
			World.MAP.BLOCKS.get(i).RenderFully(shape, sprite);
		}
	}

	public static void RenderInfo(SpriteBatch sprite) {
		Helper.GetFont().setScale(1f);
		TextureRegion te = Helper.GetTexture("coin");
		sprite.draw(te, 2, Gdx.graphics.getHeight() - te.getRegionHeight() - 2,
				te.getRegionWidth(), te.getRegionHeight());
		Helper.GetFont().draw(sprite, Float.toString(Player.Money),
				2 + te.getRegionWidth() + 3, Gdx.graphics.getHeight() - 5);

		TextBounds bounds = Helper.GetFont().getBounds(
				Integer.toString(MobWave.Wave));
		Helper.GetFont().draw(sprite, Integer.toString(MobWave.Wave),
				Gdx.graphics.getWidth() / 2 - bounds.width / 2,
				Gdx.graphics.getHeight() - 5);

		te = Helper.GetTexture("life");
		sprite.draw(te, 2,
				Gdx.graphics.getHeight() - te.getRegionHeight() - 37,
				te.getRegionWidth(), te.getRegionHeight());
		Helper.GetFont().draw(sprite, Integer.toString(Player.Health),
				2 + te.getRegionWidth() + 3, Gdx.graphics.getHeight() - 35);
	}

	public static void Tick() {
		long currentTime = System.currentTimeMillis();
		if (World.LAST_TICK <= 0)
			World.LAST_TICK = currentTime;
		if (currentTime - LAST_TICK < 1000 / 50)
			return;
		for (int i = 0; i < MOBS.size(); i++) {
			BaseMob mob = MOBS.get(i);
			if (currentTime - mob.LAST_MOVE < 1000 / mob.getSpeed())
				continue;
			mob.Move();
			mob.LAST_MOVE = currentTime;
		}

		for (int i = 0; i < TURRETS.size(); i++) {
			BaseTurret turret = TURRETS.get(i);
			if (currentTime - turret.LastTick < turret.TickDelay)
				continue;
			turret.Tick();
			turret.LastTick = currentTime;
		}
		try {
			Thread.sleep(1000 / 50);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		World.LAST_TICK = currentTime;
	}

	public static int GetMapWidth() {
		return BLOCKS_WIDE * Block.GetSize();
	}

	public static int GetMapHeight() {
		return BLOCKS_HIGH * Block.GetSize();
	}

	@Override
	public boolean keyDown(int keycode) {
		// PC Key-bindings, more info on wiki:
		// https://bitbucket.org/approved94/grid2/wiki/Home
		// W = 51
		// R = 46
		// I = 37
		// M = 41
		// O = 43
		// C = 31
		// U = 49

		if (keycode == 51) {
			Block MouseHover = BlockAtPix(Gdx.input.getX() + OFFSET_X,
					Gdx.input.getY() - OFFSET_Y, true);
			// Block MouseHover = BlockAtPix(DOWN_X, DOWN_Y, true);
			int addX = ((int) MouseHover.getX() / Block.GetSize()) + 1;
			int addY = ((int) MouseHover.getY() / Block.GetSize()) + 1;
			if (addY == 1)
				addY = 0;
			if (addX == 1)
				addX = 0;

			AddRoadPoint(addX, addY);

			Init();
		} else if (keycode == 46) {
			Block MouseHover = BlockAtPix(Gdx.input.getX() + OFFSET_X,
					Gdx.input.getY() - OFFSET_Y, true);
			MAP.RemoveRoadPoint(
					((int) MouseHover.getX() / Block.GetSize()) + 1,
					((int) MouseHover.getY() / Block.GetSize()) + 1);

			Init();
		} else if (keycode == 37) {
			Block MouseHover = BlockAtPix(Gdx.input.getX() + OFFSET_X,
					Gdx.input.getY() - OFFSET_Y, true);
			BaseTurret turret = new LaserTurret(MouseHover.getX(),
					MouseHover.getY());
			AddTurret(turret);
		} else if (keycode == 41) {
			// Make mob spawning a random thing
			int r = Helper.Random(0, 1);
			BaseMob mob = null;
			if (r == 0)
				mob = new SlimeMob();
			if (r == 1)
				mob = new FlyerMob();
			World.AddMob(mob);
		} else if (keycode == 43) {
			Block MouseHover = BlockAtPix(Gdx.input.getX() + OFFSET_X,
					Gdx.input.getY() - OFFSET_Y, true);
			BaseTurret turret = new TeslaTower(MouseHover.getX(),
					MouseHover.getY());
			AddTurret(turret);
		} else if (keycode == 81) {
			Player.ApplyMoney(10000);
		} else if (keycode == 144) {
			setScale(1);
		} else if (keycode == 31) {
			LoadMap(new Map());
		} else if (keycode == 49) {
			MAP.UploadMap();
		} else {
			System.out.println(keycode);
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {

		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		numberOfFingers++;
		if (numberOfFingers == 1) {
			fingerOnePointer = pointer;
			fingerOne.set(screenX, screenY, 0);

			super.touchDown(screenX, screenY, pointer, button);
			if (Gdx.graphics.getWidth() - screenX > TurretMenuWidth) {
				CloseTurretMenu();
				return true;
			}
		} else if (numberOfFingers == 2) {
			fingerTwoPointer = pointer;
			fingerTwo.set(screenX, screenY, 0);

			float distance = fingerOne.dst(fingerTwo);
			lastDistance = distance;
		}

		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		if (numberOfFingers == 1) {
			super.touchUp(screenX, screenY, pointer, button);
			Block MouseHover = World.GetMouseHoverBlock();

			for (int i = 0; i < TURRETS.size(); i++) {
				if (TURRETS.get(i).Within(screenX,
						Gdx.graphics.getHeight() - screenY)) {
					SELECTED_TURRET = TURRETS.get(i);
					break;
				}
			}

			if (turret != null) {
				if (turret.CanPlace()) {
					BaseTurret t = turret.New(MouseHover.getX(),
							MouseHover.getY());
					AddTurret(t);
				}
				turret = null;
				SELECTED_TURRET = null;
			} else if (SELECTED_TURRET != null) {
				boolean foundTurret = false;
				for (int i = 0; i < TURRETS.size(); i++) {
					if (TURRETS.get(i).Within(screenX,
							Gdx.graphics.getHeight() - screenY)) {
						foundTurret = true;
					}
				}
				if (!foundTurret) {
					World.SELECTED_TURRET = null;
				}
			}
		}
		numberOfFingers--;

		// Error detection and prevention
		if (numberOfFingers < 0)
			numberOfFingers = 0;

		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		if (numberOfFingers > 1) {
			// for pinch-to-zoom
			if (pointer == fingerOnePointer) {
				fingerOne.set(screenX, screenY, 0);
			}
			if (pointer == fingerTwoPointer) {
				fingerTwo.set(screenX, screenY, 0);
			}

			float distance = fingerOne.dst(fingerTwo);

			float am = 0.05f;
			if (lastDistance < distance) {
				Scale += am;
				if (Scale > 6) {
					Scale = 6;
				}
			} else if (lastDistance > distance) {
				float setScale = Scale - am;
				if (setScale < 1)
					setScale = 1;
				Scale = setScale;
			}

			// clamps field of view to common angles...
			// if (cam.fieldOfView < 10f) cam.fieldOfView = 10f;
			// if (cam.fieldOfView > 120f) cam.fieldOfView = 120f;*/

			lastDistance = distance;
		} else {
			if (turret == null) {
				// int newX = OFFSET_X - (screenX - DOWN_X);
				// int newY = OFFSET_Y + (screenY - DOWN_Y);

				int newX = (int) (OFFSET_X - ((Gdx.input.getDeltaX())));
				int newY = (int) (OFFSET_Y + Gdx.input.getDeltaY());

				if (newY > GetMapHeight() - Gdx.graphics.getHeight()) {
					newY = GetMapHeight() - Gdx.graphics.getHeight();
				}
				if (newX > GetMapWidth() - Gdx.graphics.getWidth()) {
					newX = GetMapWidth() - Gdx.graphics.getWidth();
				}

				if (newX < 0)
					newX = 0;
				if (newY < 0)
					newY = 0;

				OFFSET_X = newX;
				OFFSET_Y = newY;
			}
		}

		return true;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		float am = Scale * 0.1f;
		if (amount < 0) {
			if (Scale > 6) {
				Scale = 6;
				return false;
			}
			Scale += am;
		} else {
			Scale -= am;
			if (Scale <= 1) {
				Scale = 1;
				return false;
			}
		}
		return false;
	}

	public static Block BlockBefore(Point begin, Point end) {
		if (begin.x > end.x) {
			return BlockAt(end.x - 1, begin.y);
		} else if (begin.x < end.x) {
			return BlockAt(end.x + 1, begin.y);
		} else if (begin.y > end.y) {
			return BlockAt(begin.x, end.y + 1);
		} else if (begin.y < end.y) {
			return BlockAt(begin.x, end.y - 1);
		}
		return null;
	}

	/**
	 * Will return the block at the block_count specified
	 * 
	 * @param x
	 *            The block_num_x
	 * @param y
	 *            The block_num_y
	 * @return
	 */
	public static Block BlockAt(int x, int y) {
		return MAP.BlockAt(x, y);
	}

	/**
	 * Will return the block at given pixels
	 * 
	 * @param x
	 *            The pixel X
	 * @param y
	 *            The pixel Y
	 * @return
	 */
	public static Block BlockAtPix(float x, float y, boolean FlipY) {
		return MAP.BlockAtPix(x, y, FlipY);
	}

	public static Block BlockAtPix(float x, float y) {
		return BlockAtPix(x, y, false);
	}

	public static ArrayList<BaseMob> GetMobs() {
		return World.MOBS;
	}

	@Override
	public void Render(ShapeRenderer shape, SpriteBatch sprite) {
		render(sprite, shape);
		super.Render(shape, sprite);
	}

	public static void BuildTestMap() {
		// X Y
		Map map = new Map();
		map.AddRoadPoint(0, 0);
		map.AddRoadPoint(0, 5);
		map.AddRoadPoint(2, 5);
		map.AddRoadPoint(2, 7);
		map.AddRoadPoint(7, 7);
		map.AddRoadPoint(7, 2);
		map.AddRoadPoint(5, 2);
		map.AddRoadPoint(5, 0);
		map.AddRoadPoint(25, 0);
		map.AddRoadPoint(25, 15);
		map.AddRoadPoint(World.BLOCKS_WIDE, 15);
		LoadMap(map);
		World.Init();
	}

	public static float getScale() {
		return Scale;
	}

	public static void setScale(float scale) {
		Scale = scale;
	}

	public static Block GetMouseHoverBlock() {
		return World.BlockAtPix(Gdx.input.getX(), Gdx.input.getY(), true);
	}
}