package com.approved.world;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.approved.Point;
import com.approved.Server;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonWriter.OutputType;

public class MapJson {

	public String Name = "";
	public ArrayList<Point> Waypoints = new ArrayList<Point>();
	
	public void AddRoadPoint(int x, int y) {
		if(Waypoints == null) Waypoints = new ArrayList<Point>();
		Waypoints.add(new Point(x, y));
	}
	
	public String ToJson() {
		Json json = new Json();
		json.setOutputType(OutputType.javascript);
		return json.toJson(this);
	}
	
	public static Map FromJson(String json) {
		Json j = new Json();
		return (Map) j.fromJson(Map.class, json);
	}
	
	public static Map FromID(int id) {
		String json = Server.GetMap(id);
		System.out.println("Map json: "  +json);
		return FromJson(json);
	}
	
	public MapJson() {
		
	}
	
	public void UploadMap() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		Name = dateFormat.format(date); // This is just a reference to be able to find in when settings a name later.
		Server.UploadMap(ToJson());
	}
	
	public void RemoveRoadPoint(int x, int y) {
		if(Waypoints == null) return;
		for(int i = 0; i < Waypoints.size(); i++) {
			if(Waypoints.get(i).x == x && Waypoints.get(i).y == y) {
				Waypoints.remove(i);
			}
		}
	}
	
}
