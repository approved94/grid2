package com.approved.world;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.approved.Helper;
import com.approved.Point;
import com.approved.Server;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.Json;

public class Map {
	public MapJson mapJson = new MapJson();
	public ArrayList<Block> BLOCKS = new ArrayList<Block>();
	
	public void AddRoadPoint(int x, int y) {
		mapJson.AddRoadPoint(x, y);
	}
	
	public String ToJson() {
		return mapJson.ToJson();
	}
	
	public static Map FromJson(String json) {
		Json j = new Json();
		Map map = new Map();
		map.mapJson = (MapJson) j.fromJson(MapJson.class, json);
		return map;
	}
	
	public static Map FromID(int id) {
		String json = Server.GetMap(id);
		System.out.println("Map json: "  +json);
		return FromJson(json);
	}
	
	public Map() {
		
	}
	
	public void UploadMap() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		mapJson.Name = dateFormat.format(date); // This is just a reference to be able to find in when settings a name later.
		Server.UploadMap(ToJson());
	}
	
	public void RemoveRoadPoint(int x, int y) {
		mapJson.RemoveRoadPoint(x, y);
	}
	
	public void Init() {
		CreateBlocks();
		CreateRoad();
	}
	
	private void CreateRoad() {
		if(mapJson.Waypoints == null || mapJson.Waypoints.size() < 2) {
			System.out.println("Atleast 2 points needed to calculate road-map, canceling operation!");
			return;
		}
		
		Point last = new Point(0, 0), next = new Point(0, 0), current = new Point(0,0);
		for(int i = 0; i < mapJson.Waypoints.size()-1; i++) {
			if(i == 0) {
				last = mapJson.Waypoints.get(0);
			}else {
				last = mapJson.Waypoints.get(i - 1);
			}
			current = mapJson.Waypoints.get(i);
			if(i >= mapJson.Waypoints.size()-1) {
				next = mapJson.Waypoints.get(i);
			}else {
				next = mapJson.Waypoints.get(i+1);
			}
			
			DrawRoad(current, next);
			Block b = BlockAt(current.x, current.y);
			b.setBackground(WaypointImage(last, current, next));
			b.IS_ROAD = true;
		}
		Point l = mapJson.Waypoints.get(mapJson.Waypoints.size()-1);
		
		// Make sure the last block has a texture to fit the road.
		BlockAt(l.x, l.y).IS_ROAD = true;
		if(l.x == current.x) BlockAt(l.x, l.y).setBackground(Helper.GetTexture("roadup"));
		if(l.y == current.y) BlockAt(l.x, l.y).setBackground(Helper.GetTexture("roadright"));
	}
	
	/**
	 * Get the right wapoint image
	 * @param last Last waypoint
	 * @param i, this waypoint
	 * @param next next waypoint
	 * @return
	 */
	private TextureRegion WaypointImage(Point last, Point i, Point next) {
		// if (i.x == 0 && i.y == 0)return Helper.GetTexture("roadup");

		boolean toUp = false;
		boolean toDown = false;

		boolean toLeft = false;
		boolean toRight = false;

		boolean fromUp = false;
		boolean fromDown = false;

		boolean fromLeft = false;
		boolean fromRight = false;

		if (last.y < i.y)
			fromDown = true;
		if (last.y > i.y)
			fromUp = true;

		if (last.x < i.x)
			fromLeft = true;
		if (last.x > i.x)
			fromRight = true;

		if (i.y < next.y)
			toUp = true;
		if (i.y > next.y)
			toDown = true;

		if (next.x < i.x)
			toLeft = true;
		if (next.x > i.x)
			toRight = true;

		if (fromRight && toUp) {
			return Helper.GetTexture("turnrightup");
		} else if (fromRight && toDown) {
			return Helper.GetTexture("turnright");
		} else if (fromLeft && toUp) {
			return Helper.GetTexture("turnleftup");
		} else if (fromLeft && toDown) {
			return Helper.GetTexture("turnleft");
		} else if (fromUp && toRight) {
			return Helper.GetTexture("turnrightup");
		} else if (fromUp && toLeft) {
			return Helper.GetTexture("turnleftup");
		} else if (fromDown && toRight) {
			return Helper.GetTexture("turnright");
		} else if (fromDown && toLeft) {
			return Helper.GetTexture("turnleft");
		}

		if(toRight || toLeft) return Helper.GetTexture("roadright");
		if(toDown  || toUp) return Helper.GetTexture("roadup");
		
		return Helper.GetTexture("coin");
	}
	
	public void DrawRoad(Point begin, Point end) {
		TextureRegion te = null;
		
		if(begin.x == end.x) {
			te = Helper.GetTexture("roadup");
		}else if(begin.y == end.y) {
			te = Helper.GetTexture("roadright");
		}
		
		if(begin.x > end.x) {
			for(int x = end.x; x < begin.x; x++) {
				BlockAt(x, begin.y).setBackground(te);
				BlockAt(x, begin.y).IS_ROAD = true;
			}
		}else if(begin.x < end.x) {
			for(int x = begin.x; x < end.x; x++) {
				BlockAt(x, begin.y).setBackground(te);
				BlockAt(x, begin.y).IS_ROAD = true;
			}
		}else if(begin.y > end.y) {
			for(int y = end.y; y < begin.y; y++) {
				BlockAt(begin.x, y).setBackground(te);
				BlockAt(begin.x, y).IS_ROAD = true;
			}
		}else if(begin.y < end.y) {
			for(int y = begin.y; y < end.y; y++) {
				BlockAt(begin.x, y).setBackground(te);
				BlockAt(begin.x, y).IS_ROAD = true;
			}
		}
	}
	
	public void CreateBlocks() {
		BLOCKS.clear(); // Clear all old blocks
		
		double width = (Gdx.graphics.getWidth() / Block.GetSize()) * 1.2f;
		if(World.BLOCKS_WIDE != 0) width = World.BLOCKS_WIDE;
		double height = (Gdx.graphics.getHeight() / Block.GetSize()) * 1.2f;
		if(World.BLOCKS_HIGH != 0) height = World.BLOCKS_HIGH;
		
		// Generate all the blocks
		for(int x = 0; x < width; x++) {
			for(int y = 0; y < height; y++) {
				Block b = new Block(x * Block.GetSize(), y * Block.GetSize());
				b.setBackground(Helper.GetTexture("bg"));
				AddBlock(b);
			}
		}		
		
		World.BLOCKS_WIDE = (int) width;
		World.BLOCKS_HIGH = (int) height;
	}
	
	private void AddBlock(Block b) {
		BLOCKS.add(b);
	}

	/**
	 * Will return the block at the block_count specified
	 * @param x The block_num_x
	 * @param y The block_num_y
	 * @return
	 */
	public Block BlockAt(int x, int y) {
		return BlockAtPix(x*Block.GetSize(), y*Block.GetSize()); 
	}
	
	/**
	 * Will return the block at given pixels
	 * @param x The pixel X
	 * @param y The pixel Y
	 * @return
	 */
	public Block BlockAtPix(float x, float y, boolean FlipY) {
		if(FlipY) y = Gdx.graphics.getHeight() - y;
		for(int i = 0; i < BLOCKS.size(); i++) {
			Block b = BLOCKS.get(i);
			if(b.Inside(x, y)) return b;
		}
		System.out.println("Didn't find a block at pixels: (" + x + ", " + y + ")");
		return new Block(-100, -100); // Return a dummy-block to avoid crashes
	}
	
	public Block BlockAtPix(float x, float y) {
		return BlockAtPix(x, y, false);
	}
	
	public float GetWidth() {
		return (World.BLOCKS_WIDE * Block.GetSize());
	}
	
	public float GetHeight() {
		return (World.BLOCKS_HIGH * Block.GetSize());
	}
	
	public void Render(SpriteBatch sprite, ShapeRenderer shape) {
		World.setScale(0.65f);
		// DO NOT TOUCH VALUES BELOW!!!!
		World.OFFSET_X = (int) (((float)Gdx.graphics.getWidth() / 2f - (float)GetWidth() / 2) / -1) - (int) (Gdx.graphics.getWidth() * 0.06f);
		World.OFFSET_Y = (int) (((float)Gdx.graphics.getHeight() / 2f - (float)GetHeight() / 2) / -1) - (int) (Gdx.graphics.getHeight() * 0.06f);
		for(int i = 0; i < BLOCKS.size(); i++) {
			BLOCKS.get(i).RenderFully(shape, sprite);
		}
	}
}
