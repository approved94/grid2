package com.approved.world.mobs;

import com.approved.Helper;
import com.approved.world.Block;

public class FlyerMob extends BaseMob {

	public FlyerMob() {
		super();
		setBackground(Helper.GetTexture("Mob-two"));
		setSpeed(Block.GetBaseSize()*3);
		setBaseHealth(80);
		setWorth(25);
	}
	
	@Override
	public BaseMob New() {
		return new FlyerMob();
	}
	
}
