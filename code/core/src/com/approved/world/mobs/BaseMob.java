package com.approved.world.mobs;

import java.util.ArrayList;

import com.approved.Point;
import com.approved.player.Player;
import com.approved.world.Block;
import com.approved.world.World;
import com.approved.world.turrets.BaseTurret;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

public class BaseMob extends Block {
	private int Speed = 60; // Pixels per second to move
	private int onTheWayToIndex;
	private boolean Dead = false;
	private int BaseHealth = 100, Health = 100;
	private float degrees = 0;
	private float Worth = 0;
	private Sprite s;
	private long DistanceTraveled = 0;
	public long LAST_MOVE;

	public BaseMob() {
		super(World.MOB_SPAWN_X, World.MOB_SPAWN_Y);
	}
	
	public Point NextPoint() {
		ArrayList<Point> ROAD_POINTS = World.MAP.mapJson.Waypoints;
		if(onTheWayToIndex > ROAD_POINTS.size()-1) {
			Dead = true;
			Player.ApplyHealth(-1);
			World.RemoveMob(this);
			return ROAD_POINTS.get(ROAD_POINTS.size()-1);
		}
		float CheckX = ROAD_POINTS.get(onTheWayToIndex).x * Block.GetBaseSize();
		float CheckY = ROAD_POINTS.get(onTheWayToIndex).y * Block.GetBaseSize();
		
		if(CheckX > Block.GetBaseSize()) CheckX -= Block.GetBaseSize();
		if(CheckY > Block.GetBaseSize()) CheckY -= Block.GetBaseSize();
		
		if(getX() == CheckX && getY() == CheckY) {
			onTheWayToIndex++;
			
			Point next = NextPoint();
			float nextX = ((next.x * Block.GetBaseSize()));
			float nextY = ((next.y * Block.GetBaseSize()));
			
			if(nextX > Block.GetBaseSize()) nextX -= Block.GetBaseSize();
			if(nextY > Block.GetBaseSize()) nextY -= Block.GetBaseSize();
			
			degrees = (float) ((Math.atan2 (nextX - getX(), - (nextY - getY()))*180.0d/Math.PI) + 180.0f);
			
			return NextPoint();
		}
		return ROAD_POINTS.get(onTheWayToIndex);
	}
	
	public void Move() {
		Point next = NextPoint();
		float nextX = ((next.x * Block.GetBaseSize()));
		float nextY = ((next.y * Block.GetBaseSize()));
		
		if(nextX > Block.GetBaseSize()) nextX -= Block.GetBaseSize();
		if(nextY > Block.GetBaseSize()) nextY -= Block.GetBaseSize();
		
		//if(getX() > 0) System.out.println(nextX + ", " + nextX);
		
		if(getX() < nextX) {
			setX(getX() + 1);
			if(getX() > nextX) setX(nextX);
		}else if(getX() > nextX) {
			setX(getX() - 1);
			if(getX() < nextX) setX(nextX);
		}
		
		if(getY() < nextY) {
			setY(getY() + 1);
			if(getY() > nextY) setY(nextY);
		}else if(getY() > nextY) {
			setY(getY() - 1);
			if(getY() < nextY) setY(nextY);
		}
		DistanceTraveled++;
	}

	public boolean isDead() {
		return Dead;
	}
	
	public void Damage(float amount) {
		Health -= amount;
		if(Health <= 0) {
			Dead = true;
			World.RemoveMob(this);
			Player.ApplyMoney(getWorth());
		}
	}
	
	public int getBaseHealth() {
		return BaseHealth;
	}

	public void setBaseHealth(int baseHealth) {
		if(Health == BaseHealth) Health = baseHealth;
		BaseHealth = baseHealth;
	}
	
	public int getHealth() {
		return Health;
	}

	public void setHealth(int health) {
		Health = health;
	}

	public void DrawHealth(ShapeRenderer shape, SpriteBatch sprite) {
		sprite.end();
		shape.begin(ShapeType.Filled);
		// First we draw background
		shape.setColor(1,0,0,1);
		shape.rect(getRenderX(), getRenderY() + Block.GetSize(), Block.GetSize(), 2);
		
		shape.setColor(0,1,0,1);
		int fillWidth = (int)(((float)PercentHealth() / 100F) * (float)Block.GetSize());
		shape.rect(getRenderX(), getRenderY() + Block.GetSize(), fillWidth, 2);
		
		shape.end();
		sprite.begin();
	}
	
	public int PercentHealth() {
		return (int) (((float)Health / (float)BaseHealth) * 100F);
	}
	
	public int getSpeed() {
		return Speed;
	}

	public void setSpeed(int speed) {
		Speed = speed;
	}
	
	@Override
	public void RenderFully(ShapeRenderer shape, SpriteBatch sprite) {
		if(getBackground() != null) {
			/*Sprite s = new Sprite(getBackground());
			s.rotate(degrees);
			s.setSize(getRenderWidth(), getRenderHeight());
			s.setX(getRenderX());
			s.setY(getRenderY());
			s.draw(sprite);*/
			//sprite.draw(s, getRenderX(), getRenderY(), getRenderWidth(), getRenderHeight());
			//s.draw(sprite, getRenderX(), getRenderY(), (float)Block.GetSize(), (float)Block.GetSize());
			
			
			if(s == null) s = new Sprite(getBackground());
			s.setRotation(degrees);
			s.setOrigin(getRenderWidth() / 2, getRenderHeight() / 2);
			s.setSize(getRenderWidth(), getRenderHeight());
			s.setX(getRenderX());
			s.setY(getRenderY());
			s.draw(sprite);
		}
		
		if(getTexture() != null) sprite.draw(getTexture(), getRenderX(), getRenderY(), getWidth(), getHeight());
	}

	public BaseMob New() {
		return new BaseMob();
	}

	public float getWorth() {
		return Worth;
	}

	public void setWorth(float worth) {
		Worth = worth;
	}
	
	public float PixelsAway(BaseTurret turret) 
	{
		return (float) Math.abs(Math.sqrt((MiddleX() - turret.MiddleX()) * (MiddleX() - turret.MiddleX()) + (MiddleY() - turret.MiddleY()) * (MiddleY() - turret.MiddleY())));
	}

	public long getDistanceTraveled() {
		return DistanceTraveled;
	}

}
