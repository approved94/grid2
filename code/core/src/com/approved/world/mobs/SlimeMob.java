package com.approved.world.mobs;

import com.approved.Helper;
import com.approved.world.Block;

public class SlimeMob extends BaseMob {

	public SlimeMob() {
		super();
		
		setBackground(Helper.GetTexture("Mob-one"));
		setSpeed(Block.GetBaseSize()*1);
		setBaseHealth(150);
		setWorth(32.5f);
	}
	
	@Override
	public SlimeMob New() {
		return new SlimeMob();
	}
	
}
