package com.approved.desktop;

import com.approved.GridTD2;
import com.approved.player.Player;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 1024;
		config.height = 768;
		Player.LoadFromJson("desktopLauncher");
		new LwjglApplication(new GridTD2(false), config);
	}
}
