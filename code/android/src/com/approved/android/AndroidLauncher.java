package com.approved.android;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.WindowManager;
import com.approved.GridTD2;
import com.approved.Helper;
import com.approved.player.Player;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		initialize(new GridTD2(true), config);
		Helper.ClearTextures();
		
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				Player.LoadFromJson(LoadUID());
			}
		}).start();
	}
	
	@SuppressLint("DefaultLocale")
	public String LoadUID(){
		String re = "emptyName";
	    AccountManager manager = AccountManager.get(this); 
	    Account[] accounts = manager.getAccounts();
	    for(Account acc : accounts){
	    	if(acc.name != null && acc.name != "") re = acc.name;
	    	if(acc.name.toLowerCase().contains("@gmail.com")) return acc.name; // This is to prefer.
	    }
	    return re;
	}
}
