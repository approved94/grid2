# README #

To see what needs to be done and what is planed, please refer to Trello: https://trello.com/b/lvicKwhC/gridtd
If you have a bug/issue/proposal to report please do so at the Issues section in the repo.

### What is this repository for? ###

This repository contain the code for the Grid2 project wich is a Android Tower Defence game.

### How do I get set up? ###

Pull the repository, open Eclipse and go till File -> Import -> General -> Exsisting projects into workspace, then select the root of the pull from Git.

### Contribution guidelines ###

All help is welcome, feedback, improvments and so on.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact